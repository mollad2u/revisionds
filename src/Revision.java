public class Revision{
	
	private Matieres matiere;
	private int heureRevision;
	
	public Revision(Matieres m, int h)
	{
		this.matiere=m;
		this.heureRevision=h;
		
	}
	
	public void ajouterHeurerevision(int h){
		this.heureRevision+=h;
	}

	public String getMatiere() {
		return matiere;
	}

	public void setMatiere(String matiere) {
		this.matiere = matiere;
	}

	public int getHeureRevision() {
		return heureRevision;
	}

	public void setHeureRevision(int heureRevision) {
		this.heureRevision = heureRevision;
	}
	

	
}